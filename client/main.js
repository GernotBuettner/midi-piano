// Client entry point, imports all client code
import React from 'react';
import ReactDOM from 'react-dom';


import '/imports/startup/client';
import '/imports/startup/both';


import AppRoot from '../imports/ui/layout/home/home.js'

const Root = document.getElementById('react');
ReactDOM.render(<AppRoot />, Root)
