import React from 'react';

import './header.scss';

export default class Header extends React.Component {
  render() {
    return (
      <header>
        <h2>Flowkey</h2>
        <h1>Piano</h1>
      </header>
    )
  }
}
