import React from 'react';
import Header from '../header/header.js'

import Keyboard from '../../components/keyboard/keyboard.js'

export default class AppRoot extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Keyboard />
      </div>
    )
  }
}
