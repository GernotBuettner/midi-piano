import React from 'react';

import './keyboard.scss';
import Tiles from './tiles/tiles.js'

export default class Keyboard extends React.Component {
  render() {
    let tilesList = [
      <Tiles />,
      <Tiles />,
      <Tiles />
    ]

    return (
      <div className="keyboard-wrp">
        {tilesList}
      </div>
    )
  }
};
