import React from 'react';

export default class Tiles extends React.Component {
  render() {
    return (
      <div className="tiles-wrp">
        <div className="white-tile-wrp">
          <div className="white-tile">

          </div>
          <div className="white-tile">

          </div>
          <div className="white-tile">

          </div>
          <div className="white-tile">

          </div>
          <div className="white-tile">

          </div>
          <div className="white-tile">

          </div>
          <div className="white-tile">

          </div>
        </div>
        <div className="black-tile-wrp">
          <div className="black-tile">

          </div>
          <div className="black-tile">

          </div>
          <div className="black-tile-middle">

          </div>
          <div className="black-tile">

          </div>
          <div className="black-tile">

          </div>
        </div>
      </div>
    )
  }
};
